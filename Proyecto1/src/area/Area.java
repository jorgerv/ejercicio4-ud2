package area;

import java.util.Scanner;

public class Area {

	public static void main(String[] args) {
		Scanner teclado=new Scanner(System.in);
		
		System.out.println("Introduce el radio del c�rculo:");
		int radio=teclado.nextInt();
		
		double area=Math.PI*Math.pow(radio, 2);
		System.out.println("El area del circulo es: "+area);
		
		teclado.close();
	}

}